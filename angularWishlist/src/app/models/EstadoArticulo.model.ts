import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Articulo } from './Articulo.model'

//Estado o Store - Define el estado de la aplicación, es una interface de atributos tipados
export interface EstadoArticulo{
    items: Articulo[] //Listado de articulo
    loading: boolean //Variable que se pone en true cuando se llama, por ejemplo, un ajax async.
    favorites: Articulo //Item seleccionado como favorito
}

//Este es inicializador de Estados, ya que en algun momento este debe inicializarse
export const initializeEstadoArticulo = function(){
    return {
        items: [],
        loading: false,
        favorites: null
    }
}

//Acciones - Acciones de la app (interacción de Usuario en GUI) que disparan un cambio de Estado, implementan
//la interfaz Action y tienen que tener una variable del tipo type, que idealmente debe ser definida en
//un string dentro de un enum
export enum EstadoArticuloTypes {
    NUEVO_ARTICULO = '[Articulo] Nuevo',
    ELEGIDO_FAVORITO = '[Articulo] Favorito',
    VOTE_UP = '[Articulo] Vote Up',
    VOTE_DOWN = '[Articulo] Vote Down'
}

export class NuevoArticuloAction implements Action {
    type = EstadoArticuloTypes.NUEVO_ARTICULO;
    constructor(public art: Articulo){ }
}

export class ElegidoFavoritoAction implements Action {
    type = EstadoArticuloTypes.ELEGIDO_FAVORITO;
    constructor(public art: Articulo){ }
}

export class VoteUpAction implements Action {
    type = EstadoArticuloTypes.VOTE_UP;
    constructor(public art: Articulo) {}
}

export class VoteDownAction implements Action {
    type = EstadoArticuloTypes.VOTE_DOWN;
    constructor(public art: Articulo) {}
}

//Buena practica, se agrupan mediante un pipe todos los tipos de Acciones de cambio de Estado
export type ArticuloActions = NuevoArticuloAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction;

//Las acciones son disparadas (dispatched) para incidir sobre el Store o State , de ahi Redux llama a los
//reducers uno a uno en el orden que aparecen en el código

//Reducers
export function reducerArticulo(state: EstadoArticulo, action: ArticuloActions): EstadoArticulo{
    switch (action.type){
        case EstadoArticuloTypes.NUEVO_ARTICULO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoArticuloAction).art]
            }
        }
        case EstadoArticuloTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(art => art.setSelected(false))
            let fav: Articulo = (action as ElegidoFavoritoAction).art
            fav.setSelected(true)
            return {
                ...state,
                favorites: fav
            }
        }
        case EstadoArticuloTypes.VOTE_UP: {
            const articulo: Articulo = (action as VoteUpAction).art
            articulo.voteUp()
            return {
                ...state
            }
        }
        case EstadoArticuloTypes.VOTE_DOWN: {
            const articulo: Articulo = (action as VoteDownAction).art
            articulo.voteDown()
            return {
                ...state
            }
        }
    }
    return state;
}
/*En este caso, el Reducer es un switch-case que toma distintas acciones dependiendo del Estado, realiza
sus acciones correspondientes de acuerdo al cambio de Estado, por convención, si ninguna acción coincide
con los reducers, devuelven el estado sin mutar.*/
//---------------------------------------------------------------------

//Effects - Agregado de Angular para complemntar la reactividad anteriormente descrita, que es de Redux.

//Luego de que todos los reducers son llamados y ejecutados, esa accion son pasados a los Effects registrados
//en el sistema con el objetivo de registrar una nueva acción como consecuencia de otra acción, es decir:

/*
Reducers:
-Dada una acción y Estado del Sistema, generan un nuevo Estado, sin mutar o mutado

Effects:
-Dada una acción ofType (se filtra el tipo de accion que interesa), se genera una nueva acción, el Estado
no es mutado.
*/
@Injectable()
export class ArticuloEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(EstadoArticuloTypes.NUEVO_ARTICULO),
        map((action: NuevoArticuloAction) => new ElegidoFavoritoAction(action.art))
    )

    constructor(private actions$: Actions) {}
}