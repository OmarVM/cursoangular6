export class Articulo {
    
    public selected: boolean
    nombre: string
    marca: string
    id: number
    public caracteristicas: String[]

    constructor(nombre: string, marca: string, public votes: number = 0){
        this.nombre = nombre
        this.marca = marca
        this.caracteristicas = ['Economico','Alto rendimiento']
    }

    public isSelected(): boolean{
        return this.selected;
    }

    public setSelected(selected: boolean){
        /*
        if (this.selected && selected){
            this.selected = !selected
        } else{
            this.selected = selected
        }*/

        this.selected = selected;
    }
    /*
    constructor(public nombre: string, public marca: string){
        //Esta linea de codigo equivale a la porcion anterior
    }*/

    public voteUp() {
        this.votes++;
    }

    public voteDown() {
        this.votes--;
    }
} 