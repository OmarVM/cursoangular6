import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppState } from '../app.module';
import { Articulo } from './Articulo.model';
import { ElegidoFavoritoAction, NuevoArticuloAction } from './EstadoArticulo.model';

@Injectable()
export class ArticulosApiClient {
    //articulos:Articulo[];
    //current: Subject<Articulo> = new BehaviorSubject<Articulo>(null);

    constructor(private store: Store<AppState>) {
        //this.articulos = [];
    }

    add(art:Articulo){
        //this.articulos.push(art);
        this.store.dispatch(new NuevoArticuloAction(art))
    }
    /*
    getAll(): Articulo[] {
        return this.articulos;
    }
    
    
    getById(id: String): Articulo {
        return this.articulos.filter(function(d){ return d.id.toString() === id; })[0];
    }*/

    elegir(art: Articulo){
        /*
        this.articulos.forEach(articulo => {
            articulo.setSelected(false)
        })
        art.setSelected(true)
        this.current.next(art)*/
        this.store.dispatch(new ElegidoFavoritoAction(art))
    }
    /*
    subscribeOnChange(fn){
        this.current.subscribe(fn)
    }*/
}