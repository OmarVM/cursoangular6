import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Articulo } from '../models/Articulo.model';
import { VoteDownAction, VoteUpAction } from '../models/EstadoArticulo.model';

@Component({
  selector: 'app-articulo-card',
  templateUrl: './articulo-card.component.html',
  styleUrls: ['./articulo-card.component.css']
})
export class ArticuloCardComponent implements OnInit {
  
  @Input() articulo: Articulo
  @Input('idx') position: number
  @HostBinding('attr.class') cssClass = "col-md-4"
  @Output() clicked: EventEmitter<Articulo>

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter<Articulo>()
   }

  public go() {
    this.clicked.emit(this.articulo)
    return false;
  }
    
  ngOnInit(): void {
  }

  public voteUp() {
    this.store.dispatch(new VoteUpAction(this.articulo))
    return false
  }

  public voteDown() {
    this.store.dispatch(new VoteDownAction(this.articulo))
    return false
  }

}
