import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Articulo } from '../models/Articulo.model';
import { ArticulosApiClient } from '../models/ArticulosApiClient.model'
import { ElegidoFavoritoAction, NuevoArticuloAction } from '../models/EstadoArticulo.model';

@Component({
  selector: 'app-lista-articulos',
  templateUrl: './lista-articulos.component.html',
  styleUrls: ['./lista-articulos.component.css']
})
export class ListaArticulosComponent implements OnInit {

  @Output() onItemsAdded: EventEmitter<Articulo>
  updates: string[]
  all;
  //articulos: Articulo[]
  /*
  constructor() {
    this.articulos = []
   }*/

   constructor(public articulosApiClient: ArticulosApiClient, private store: Store<AppState>){
    
     //this.onItemsAdded = new EventEmitter()
     this.updates = []
    
     this.store.select(state => state.articulos.favorites)
      .subscribe(data => {
        if (data != null){
          this.updates.push('Se ha elegido a: ' + data.nombre)
        }
      })
      
     //this.articulosApiClient.subscribeOnChange((art: Articulo) => {
       /*
      if (art != null){
         this.updates.push('Se ha elegido a: ' + art.nombre)
       }*/
     //})

     this.all = store.select(state => state.articulos.items).subscribe(items => this.all = items)
   }

  ngOnInit(): void {
  }

    //Metodos Personalizados
    /*
    guardar(nombre: string, marca: string):boolean{
      
      this.articulos.push(new Articulo(nombre, marca))

      return false
    }*/

    agregado(art: Articulo){
      this.articulosApiClient.add(art)
      this.onItemsAdded.emit(art)
      this.store.dispatch(new NuevoArticuloAction(art));
    }

    /*
    elegido(articulo: Articulo){

      this.articulos.forEach(function(art){
        art.setSelected(false);
      })

      articulo.setSelected(true);
    }*/

    elegido(art: Articulo){
      this.articulosApiClient.elegir(art);
      this.store.dispatch(new ElegidoFavoritoAction(art));

      art.setSelected(true);
    }

    getAll() {
      
    }

}
