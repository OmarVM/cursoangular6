import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import { AppComponent } from './app.component';
import { ArticuloCardComponent } from './articulo-card/articulo-card.component';
import { ListaArticulosComponent } from './lista-articulos/lista-articulos.component';
import { DetalleArticuloComponent } from './detalle-articulo/detalle-articulo.component';
import { FormularioArticuloComponent } from './formulario-articulo/formulario-articulo.component';
import { ArticulosApiClient } from './models/ArticulosApiClient.model'
import { ArticuloEffects, EstadoArticulo, initializeEstadoArticulo, reducerArticulo } from './models/EstadoArticulo.model';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'home', component: ListaArticulosComponent },
  {path: 'articulo/:id', component: DetalleArticuloComponent }
];

//_Redux init
export interface AppState { 
//Define un estado Global de la App, compuesto por el Estado de los diferentes features
  articulos: EstadoArticulo

}

const reducers: ActionReducerMap<AppState> = {
  articulos: reducerArticulo
}

let reducersInitialState = {
  articulos: initializeEstadoArticulo()
}
//Redux fin init

@NgModule({
  declarations: [
    AppComponent,
    ArticuloCardComponent,
    ListaArticulosComponent,
    DetalleArticuloComponent,
    FormularioArticuloComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, 
      {
        initialState: reducersInitialState,
        runtimeChecks: {
          strictStateImmutability: false,
          strictActionImmutability: false
        }
      }),
    EffectsModule.forRoot([ArticuloEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [ArticulosApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
