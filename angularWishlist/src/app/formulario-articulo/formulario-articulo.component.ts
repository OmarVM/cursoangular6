import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { Articulo } from '../models/Articulo.model';

@Component({
  selector: 'app-formulario-articulo',
  templateUrl: './formulario-articulo.component.html',
  styleUrls: ['./formulario-articulo.component.css']
})
export class FormularioArticuloComponent implements OnInit {

  @Output() onItemsAdded: EventEmitter<Articulo>
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(private fb: FormBuilder) {
    
    this.onItemsAdded = new EventEmitter()
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nameValidator,
        this.nameValidatorParameterized(this.minLongitud)
      ])],
      marca: ['', Validators.required]
    })

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio de formulario', form)
    })
   }


  ngOnInit(): void {
    let element = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(element, 'input')
      .pipe(
        map((event: KeyboardEvent) => (event.target as HTMLInputElement).value),
        filter(txt => (txt.length >= 4)),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => {
        this.searchResults = ajaxResponse.response;
      })
  }

  guardar(nombre: string, marca: string): boolean{
    let art = new Articulo(nombre,marca);
    this.onItemsAdded.emit(art)
    return false
  }

  //Wrapper para los elementos FormControl usados en el HTML del formulario
  public toFormControl(point: AbstractControl): FormControl {
    return point as FormControl;
   }

   public nameValidator(control: FormControl): {[s: string]: boolean}{
    const longitud = control.value.toString().trim().length;
    if (longitud > 0 && longitud < 5){
      return { invalidNombre: true }
    } else {
      return null
    }
   }

   public nameValidatorParameterized(minLongitud: number): ValidatorFn{
     return (control: FormControl): {[s: string]: boolean} | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < minLongitud){
        return { minLongNombre: true }
      } else {
        return null
      }
     }
   }
}
